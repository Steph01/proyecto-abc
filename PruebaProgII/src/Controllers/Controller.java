
package controllers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;
import models.Modelo;
import views.MFrame;


public class Controller {
    private MFrame fr;
    private JFileChooser fc;
    private Modelo modelo;

    public Controller(MFrame frame) {
        this.fr = frame;
        fc = new JFileChooser();
        fc.setFileFilter(new FileNameExtensionFilter("txt files (*.txt)", "txt"));
    }    
    
    
    public void ButtonEvent(String actionCommand) {
        switch(actionCommand) {
            case "Select":
                fc.showOpenDialog(fr);  
                modelo = selectProduct(fc.getSelectedFile());
                fr.setProductosD(modelo);
                break;
                
            case "Clean":
                fr.Clean();
                break;
                
            case "Save":
                fc.showSaveDialog(fr);
                modelo = fr.getProductosD();
                saveProduct(fc.getSelectedFile());
                break;
                
        }
    }
    
    public void saveProduct(File file) {
         try {
            ObjectOutputStream w = new ObjectOutputStream(new FileOutputStream(file + ".txt")); 
            w.writeObject(modelo);
            w.flush();
        } catch (IOException ex) {
        }
    }
    
    private Modelo selectProduct(File file){
         try{
            ObjectInputStream ois =  new ObjectInputStream(new FileInputStream(file));
            return (Modelo)ois.readObject();
        } 
        catch(FileNotFoundException e){
            
        } catch (IOException | ClassNotFoundException ex) {
        }
         return null;
    }
}
