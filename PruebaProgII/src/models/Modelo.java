
package models;

import java.io.Serializable;

public class Modelo implements Serializable{
    private int productoId;
    private String productoNombre;
    private Double productoPrecio;
    public static final int id[] = {1,2,3,4};

    public Modelo() {
        
    }

    public Modelo(int productoId, String productoNombre, Double productoPrecio) {
        this.productoId = productoId;
        this.productoNombre = productoNombre;
        this.productoPrecio = productoPrecio;
    }

    public int getProductoId() {
        return productoId;
    }

    public void setProductoId(int productoId) {
        this.productoId = productoId;
    }

    public String getProductoNombre() {
        return productoNombre;
    }

    public void setProductoNombre(String productoNombre) {
        this.productoNombre = productoNombre;
    }

    public Double getProductoPrecio() {
        return productoPrecio;
    }

    public void setProductoPrecio(Double productoPrecio) {
        this.productoPrecio = productoPrecio;
    }
    
    
    
    
    
}
